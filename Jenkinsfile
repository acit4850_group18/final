/**
 * Jenkinsfile
 * Name: Goutam Thukral
 * Date: 13-12-2023
 *
 * Description:
 * This Jenkinsfile defines a pipeline with multiple stages for a Python project.
 * It has 5 stages i.e. Build, Code Quality, Code Quantity, Run Target, and Package.
 * The stages are for installing dependencies, code quality checks, code quantity measurement,
 * running target scripts, and packaging the project into a zipfile. 
 * Note - The student number and group number has been added to the 'Code Quantity' Stage.
 */

pipeline {
    agent any

    parameters {
        string(name: 'TARGET', defaultValue: 'run', description: 'Run Target Scripts')
    }

    stages {
        stage('Build') {
            steps {
                script {
                    echo 'Installing the Requirements'
                    sh 'pip install -r requirements.txt --break-system-packages'
                    echo 'Done Installing Requirements.'
                }
            }
        }

        stage('Code Quality') {
            steps {
                sh 'pylint --fail-under 5.0 *.py'
            }
        }

        stage('Code Quantity') {
            steps {
                script {
                    def pythonFileCount = sh(script: 'ls *.py | wc -l', returnStdout: true)
                    echo "Number of Python files in the project: ${pythonFileCount}"
                    echo "Student Number: A01283117"
                    echo "Group Number: 28"
                }
            }
        }

        stage('Run Target') {
            when {
                expression { params.TARGET == 'run' }
            }
            steps {
                script {
                    sh "python3 main.py phone text output"
                    sh "python3 main.py tablet csv output"
                    sh "python3 main.py laptop json output"
                    sh "python3 main.py phone yaml output"
                }
            }
        }

        stage('Package') {
            steps {
                sh 'zip -r package.zip *.py'
                // Archives the zip file as an artifact
                archiveArtifacts artifacts: 'package.zip', allowEmptyArchive: true
            }
        }
    }
}

